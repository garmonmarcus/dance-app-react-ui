import React from 'react';
import logo from './logo.svg';
import './App.css';
import {Switch, Route} from 'react-router-dom';

import Header from './components/Header/Header';

import HomePage from './pages/Home/HomeView';
import Checkout from "./pages/Checkout/Checkout";
import LoginForm from "./pages/Login/LoginForm";
import RegistrationForm from "./pages/Registration/RegistrationForm";
import Footer from "./components/Footer/Footer";
import ProductDisplay from "./pages/Product/ProductDisplay";
import VimeoUpload from "./pages/Admin/VimeoUpload";
import InstructorAdder from "./pages/Admin/InstructorAdder";
import VideoDisplay from "./pages/Video/VideoDisplay";
import InstructorPage from "./pages/Instructor/Instructor";

function App() {
    return (
        <div className="App">
            <Header/>
            <Switch>
                {/*<Route exact path='/' component={HomePage}/>*/}
                <Route exact path='/' component={ProductDisplay} />
                <Route exact path="/watch" component={HomePage} />
                <Route path='/watch/video' render={(props) => <VideoDisplay {...props} />} />
                <Route path='/subscribe' render={(props) => <Checkout {...props}  />}/>
                <Route path='/login' render={(props) => <LoginForm/>}/>
                <Route path='/registration' render={(props) => <RegistrationForm/>}/>
                <Route path='/admin/upload/video' component={VimeoUpload} />
                <Route path='/admin/add/instructor' component={InstructorAdder} />
                <Route path='/instructor' component={ InstructorPage } />

            </Switch>
            <Footer/>
        </div>
    );
}

export default App;
