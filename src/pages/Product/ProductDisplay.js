import withProductRoot from "./withProductRoot";
import React from 'react';
import {Button} from "@material-ui/core";
import Link from "@material-ui/core/Link";
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';

import HeroBg from '../../static/img/hero-bg.png';
// import HeroBgPhoto from '../../static/img/salsa-dancing-bg.jpg';
import HeroBgPhoto from '../../static/img/Social-Dance-Life.png';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
    hero: {
        color: "white",
        paddingTop: theme.spacing(1),
        // paddingBottom: theme.spacing(2),
        backgroundColor: "#23292f",
        backgroundImage: `url(${HeroBg})`
    },
    heroText: {
        width: "83%",
        textAlign: "center",
        margin: "54px auto 24px auto"
    },
    heroHeader: {
        font: "40px/1.2em 'montserrat-regular', sans-serif"
    },
    heroSubtext: {
        color: "#676e73",
        font: "16px/30px 'opensans-regular', sans-serif",
        marginTop: theme.spacing(2),
        marginRight: theme.spacing(8),
        marginLeft: theme.spacing(8)
    },
    heroImage: {
        verticalAlign: "bottom",
        display: "block"
    },
    subscribeButton: {
        // marginTop: theme.spacing(3),
        marginBottom: theme.spacing(2),
        marginRight: theme.spacing(2)

    },
    moreInfoButton: {
        // marginTop: theme.spacing(3),
        marginBottom: theme.spacing(2),
        marginLeft: theme.spacing(2),
        color: theme.palette.secondary.dark
    }
}));

function SubscribeButton() {
    const classes = useStyles();

    return (
        <Button
            size="large"
            href="/subscribe"
            color="primary"
            variant={"contained"}
            className={classes.subscribeButton}>
            Subscribe
        </Button>)
}

function ProductDisplay() {

    const classes = useStyles();

    return (
        <React.Fragment>
            <section id="hero" className={classes.hero}>
                <Container maxWidth="md">
                    <div className={classes.heroText}>
                        <Typography className={classes.heroHeader}>
                            Salsa Academy! Level up your salsa game to the Max
                        </Typography>

                        <Typography className={classes.heroSubtext}>
                            Aenean condimentum, lacus sit amet luctus lobortis, dolores et quas molestias excepturi enim
                            tellus
                            ultrices elit, amet consequat enim elit noneas sit amet luctu.
                            Quis nostrum exercitationem ullam corporis suscipit laboriosam
                        </Typography>
                    </div>

                    <div className="buttons">

                        <Link component={SubscribeButton} href="/subscribe"> </Link>

                        <Button size="large" color="inherit" variant={"contained"} className={classes.moreInfoButton}>
                            <Link href="#features" color="inherit" underline="none">Learn More</Link>
                        </Button>

                    </div>
                </Container>
                <div className={classes.heroImage}>
                    <img src={HeroBgPhoto} alt=""/>
                </div>
            </section>

            <section id='features'>

                <div className="row feature design">

                    <div className="six columns right">
                        <h3>Simple, Clean and Modern Design.</h3>

                        <p>Lorem ipsum dolor sit amet, ea eum labitur scripserit, illum complectitur deterruisset at
                            pro. Odio quaeque reformidans est eu, expetendis intellegebat has ut, viderer invenire ut
                            his. Has molestie percipit an. Falli volumus efficiantur sed id, ad vel noster propriae. Ius
                            ut etiam vivendo, graeci iudicabit constituto at mea. No soleat fabulas prodesset vel, ut
                            quo solum dicunt.
                            Nec et amet vidisse mentitum. Cibo mutat nulla ei eam.
                        </p>
                    </div>

                    <div className="six columns feature-media left">
                        <img src="images/feature-image-1.png" alt=""/>
                    </div>

                </div>

                <div className="row feature responsive">

                    <div className="six columns left">
                        <h3>Fully Responsive.</h3>

                        <p>Aenean condimentum, lacus sit amet luctus lobortis, dolores et quas molestias excepturi
                            enim tellus ultrices elit, amet consequat enim elit noneas sit amet luctu.
                            Quis nostrum exercitationem ullam corporis suscipit laboriosam.Our library is continually
                            refreshed with the latest on web technology
                            so you'll never fall behind. Quis nostrum exercitationem ullam corporis suscipit laboriosam.
                        </p>
                    </div>

                    <div className="six columns feature-media right">
                        <img src="images/feature-image-2.png" alt=""/>
                    </div>

                </div>

                <div className="row feature cross-browser">

                    <div className="six columns right">
                        <h3>Cross-Browser Compatible.</h3>

                        <p>Lorem ipsum dolor sit amet, ea eum labitur scripserit, illum complectitur deterruisset at
                            pro. Odio quaeque reformidans est eu, expetendis intellegebat has ut, viderer invenire ut
                            his. Has molestie percipit an. Falli volumus efficiantur sed id, ad vel noster propriae. Ius
                            ut etiam vivendo, graeci iudicabit constituto at mea. No soleat fabulas prodesset vel, ut
                            quo solum dicunt.
                            Nec et amet vidisse mentitum. Cibo mutat nulla ei eam.
                        </p>
                    </div>

                    <div className="six columns feature-media left">
                        <img src="images/feature-image-3.png" alt=""/>
                    </div>

                </div>

                <div className="row feature video">

                    <div className="six columns left">
                        <h3>Video Support.</h3>

                        <p>Aenean condimentum, lacus sit amet luctus lobortis, dolores et quas molestias excepturi
                            enim tellus ultrices elit, amet consequat enim elit noneas sit amet luctu.
                            Quis nostrum exercitationem ullam corporis suscipit laboriosam. No soleat fabulas prodesset
                            vel, ut quo solum dicunt. Nec et amet vidisse mentitum. Cibo mutat nulla ei eam.
                        </p>
                    </div>

                    <div className="six columns feature-media right">
                        <div className="fluid-video-wrapper">
                            <iframe
                                src="http://player.vimeo.com/video/14592941?title=0&amp;byline=0&amp;portrait=0&amp;color=F64B39"
                                width="500" height="281" frameBorder="0" webkitallowfullscreen mozallowfullscreen
                                allowFullScreen></iframe>
                        </div>
                    </div>
                </div>
            </section>

            <section id="pricing">

                <div className="row section-head">

                    <h1>Pricing Tables.</h1>

                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium,
                        totam rem aperiam,
                        eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt
                        explicabo. Nemo enim ipsam
                        voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores
                        eos qui ratione
                        voluptatem sequi nesciunt.
                    </p>

                </div>
            </section>
        </React.Fragment>
    )
}

export default withProductRoot(ProductDisplay);