import withRoot from "../../withRoot";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
    useHistory,
} from "react-router-dom";
import React from 'react';
import {Container} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Typography from '@material-ui/core/Typography';
import Button from "@material-ui/core/Button";
import Modal from '@material-ui/core/Modal';
import {addInstructor} from "../../utils/api/apiFetchHooks";
import {sleep} from "../../utils/UtilFunctions/PromiseActions";

const useStyles = makeStyles(theme => ({
    container: {
        marginTop: theme.spacing(8),
    },
    textField: {
        marginTop: theme.spacing(2),
        marginRight: theme.spacing(96),
        textAlign: "left"
    },
    button: {
        marginTop: theme.spacing(2),
        marginRight: theme.spacing(96),
    },
    paper: {
        position: 'absolute',
        width: 400,
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    }
}));

function rand() {
    return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
    const top = 50 + rand();
    const left = 50 + rand();

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}

function InstructorAdder() {

    const classes = useStyles();
    let history = useHistory();

    const [instructorName, setInstructorName] = React.useState('');
    const [aboutInstructor, setAboutInstructor] = React.useState('');
    const [modalStyle] = React.useState(getModalStyle);
    const [open, setOpen] = React.useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        setAboutInstructor('');
        setInstructorName('');
    };

    const onChangeHandler = (event) => {
        const name = event.target.name;
        const value = event.target.value;

        switch (name) {
            case "name":
                setInstructorName(value);
                break;
            case "about":
                setAboutInstructor(value);
                break;
            default:
                break;
        }
    };

    const onSubmit = async () => {
        await sleep(300);

        let instructorForm = {
            name: instructorName,
            about: aboutInstructor
        };

        let instructorAdded = await addInstructor(instructorForm);

        if (instructorAdded.message.toLowerCase() === "success") {
            handleOpen();
            // history.push("/")
        }
    };

    return (
        <Container className={classes.container}>

            <Typography variant="h1">Add New Instructor</Typography>

            <FormControl fullWidth>
                <TextField
                    className={classes.textField}
                    name={"name"}
                    value={instructorName}
                    id="textfield-name"
                    label="Instructor Name"
                    variant="outlined"
                    onChange={onChangeHandler}
                />
            </FormControl>

            <FormControl fullWidth>
                <TextField
                    className={classes.textField}
                    name={"about"}
                    value={aboutInstructor}
                    id="textfield-about"
                    label="About the Instructor"
                    variant="outlined"
                    onChange={onChangeHandler}
                />
            </FormControl>

            <Button
                className={classes.button}
                variant="contained"
                size="large"
                color="secondary"
                fullWidth
                disableElevation
                onClick={onSubmit}
            >
                Add Instructor
            </Button>

            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={open}
                onClose={handleClose}
            >
                <div style={modalStyle} className={classes.paper}>
                    <h2 id="simple-modal-title">Instructor Added Successfully</h2>

                    <Button onClick={handleClose}>Add Another</Button>
                    <Button onClick={() => {history.push("/")}}>OK</Button>
                </div>
            </Modal>
        </Container>
    )
}

export default withRoot(InstructorAdder);
