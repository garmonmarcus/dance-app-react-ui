import withRoot from "../../withRoot";
import {useHistory} from "react-router-dom";
import React, {useCallback} from 'react';
import {Container} from "@material-ui/core";
import VimeoDropzone from "../../components/DropZone/DropZone";
import {makeStyles} from "@material-ui/core/styles";
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import Button from "@material-ui/core/Button";
import Chip from '@material-ui/core/Chip';
import {addVideo, useFetchInstructors} from "../../utils/api/apiFetchHooks";
import {sleep} from "../../utils/UtilFunctions/PromiseActions";
import Modal from "@material-ui/core/Modal";

const useStyles = makeStyles(theme => ({
    container: {
        marginTop: theme.spacing(8),
    },
    textField: {
        marginTop: theme.spacing(2),
        marginRight: theme.spacing(96),
        textAlign: "left"
    },
    selectField: {
        marginTop: theme.spacing(2),
        marginRight: theme.spacing(96),
        textAlign: "left"
    },
    selectFieldInst: {
        marginTop: theme.spacing(2),
        width: theme.spacing(48)
        // marginRight: theme.spacing(96),
        // textAlign: "left"
    },
    selectLabel: {
        marginTop: theme.spacing(2),
        marginRight: theme.spacing(96),
        textAlign: "left"
    },
    instructor: {
        marginTop: theme.spacing(2),
        textAlign: "left"

    },
    tag: {
        marginTop: theme.spacing(2),
        textAlign: "left",
    },
    button: {
        marginTop: theme.spacing(6),
        // marginRight: theme.spacing(96),
    },
    modalPaper: {
        position: 'absolute',
        width: 400,
        backgroundColor: theme.palette.background.paper,
        border: '2px solid #000',
        boxShadow: theme.shadows[5],
        padding: theme.spacing(2, 4, 3),
    }
}));

function rand() {
    return Math.round(Math.random() * 20) - 10;
}

function getModalStyle() {
    const top = 50 + rand();
    const left = 50 + rand();

    return {
        top: `${top}%`,
        left: `${left}%`,
        transform: `translate(-${top}%, -${left}%)`,
    };
}

function VimeoUpload() {

    const classes = useStyles();
    let history = useHistory();

    let fetchInstructors = useFetchInstructors();

    const inputLabel = React.useRef(null);
    const instructorInputLabel = React.useRef(null);
    const [labelWidth, setLabelWidth] = React.useState(0);
    const [instructorLabelWidth, setInstructorLabelWidth] = React.useState(0);

    React.useEffect(() => {
        setLabelWidth(inputLabel.current.offsetWidth);
        setInstructorLabelWidth(instructorInputLabel.current.offsetWidth);
    }, []);

    const [videoName, setVideoName] = React.useState('');
    const [videoFile, setVideoFile] = React.useState({});
    const [videoDescription, setVideoDescription] = React.useState('');
    const [videoNotes, setVideoNotes] = React.useState('');
    const [videoLevel, setVideoLevel] = React.useState('1');
    const [videoType, setVideoType] = React.useState('');
    const [videoInstructors, setVideoInstructors] = React.useState([]);
    const [videoTags, setVideoTags] = React.useState([]);
    const [videoInstructor, setVideoInstructor] = React.useState('');
    const [videoTag, setVideoTag] = React.useState('');

    const [modalStyle] = React.useState(getModalStyle);
    const [open, setOpen] = React.useState(false);

    const handleOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
        setVideoDescription('');
        setVideoName('');
        setVideoNotes('');
        setVideoLevel('1');
        setVideoType('');
        setVideoInstructors([]);
        setVideoTags([]);
        setVideoInstructor('');
        setVideoTag('');
    };

    const onChangeHandler = (event) => {
        const name = event.target.name;
        const value = event.target.value;

        switch (name) {
            case "name":
                setVideoName(value);
            case "desc":
                setVideoDescription(value);
                break;
            case "notes":
                setVideoNotes(value);
                break;
            case "level":
                setVideoLevel(value);
                break;
            case "type":
                setVideoType(value);
                break;
            case "instructor":
                setVideoInstructor(value);
                break;
            case "tag":
                setVideoTag(value);
                break;
            default:
                break;
        }
    };

    const onDrop = useCallback((file) => {
        console.log(file);

        // setVideoName(file.name);
        setVideoFile(file);

    }, []);

    const addInstructors = () => {
        console.log("Instructor: ", videoInstructor);
        if (videoInstructor === '') return;

        // fetchInstructors

        let filteredInstructor = fetchInstructors.find(i => i.name=videoInstructor);

        console.log("Fetch Instructors : ", fetchInstructors);
        console.log("Filter Instructor : ", filteredInstructor);


        let instructorsCopy = videoInstructors;
        instructorsCopy.push(filteredInstructor);
        setVideoInstructors(instructorsCopy);

        setVideoInstructor('');
    };

    const addTags = () => {
        console.log("Adding Tags");
        console.log("Video Tags: ", videoTag);
        if (videoTag === '') return;
        let tags = videoTags;
        tags.push({name: videoTag});
        setVideoTags(tags);

        setVideoTag('');
    };

    const handleDelete = () => {
        console.info('You clicked the delete icon.');
    };

    const onSubmit = async () => {
        await sleep(300);

        let videoForm = {
            name: videoName,
            desc: videoDescription,
            notes: videoNotes,
            level: videoLevel,
            type: videoType,
            instructors: videoInstructors,
            tags: videoTags,
            // file: videoFile,
            license: '',
            privacyView: 'privacyView',
            privacyEmbed: 'whitelist'
        };

        const formData = new FormData();

        formData.append('file', videoFile);
        formData.append('video', JSON.stringify(videoForm));

        console.log("Video Form: ", formData);

        let videoAdded = await addVideo(formData);

        if (videoAdded.message.toLowerCase() === "success") {
            handleOpen();
            // history.push("/")
        }
    };

    return (
        <Container className={classes.container}>

            <Typography variant="h1">Upload New Video</Typography>

            <VimeoDropzone onChange={onDrop}/>

            <FormControl fullWidth>
                <TextField
                    className={classes.textField}
                    name={"name"}
                    id="standard-basic"
                    label="Video Name"
                    variant="outlined"
                    value={videoName}
                    onChange={onChangeHandler}
                />
            </FormControl>

            <FormControl fullWidth>
                <TextField
                    className={classes.textField}
                    name={"desc"}
                    id="standard-basic"
                    label="Video Description"
                    variant="outlined"
                    value={videoDescription}
                    onChange={onChangeHandler}
                />
            </FormControl>

            <FormControl fullWidth>
                <TextField
                    className={classes.textField}
                    name={"notes"}
                    id="standard-basic"
                    label="Video Notes"
                    variant="outlined"
                    value={videoNotes}
                    onChange={onChangeHandler}
                />
            </FormControl>

            <FormControl fullWidth>
                <TextField
                    className={classes.textField}
                    name={"type"}
                    id="standard-basic"
                    label="Video Type"
                    variant="outlined"
                    value={videoType}
                    onChange={onChangeHandler}
                />
            </FormControl>

            <FormControl fullWidth variant="outlined">
                <InputLabel
                    ref={inputLabel}
                    id="level-select-label"
                    className={classes.selectLabel}
                >
                    Dancer Level
                </InputLabel>

                <Select className={classes.selectField}
                        name={"level"}
                        labelId="level-select-label"
                        id="level-select"
                        labelWidth={labelWidth}
                        value={videoLevel}
                        onChange={onChangeHandler}
                >
                    <MenuItem value={"1"}>Beginner</MenuItem>
                    <MenuItem value={"2"}>Adv. Beginner</MenuItem>
                    <MenuItem value={"3"}>Intermediate</MenuItem>
                    <MenuItem value={"4"}>Adv. Intermediate</MenuItem>
                    <MenuItem value={"5"}>Advanced</MenuItem>
                </Select>
            </FormControl>

            <div>
                <Typography variant="h4" className={classes.instructor}>Instructors</Typography>
                <FormControl fullWidth variant="outlined" style={{textAlign: 'left', marginTop: "1%"}}>
                    <InputLabel
                        ref={instructorInputLabel}
                        id="instructor-select-label"
                        className={classes.selectLabel}
                    >
                        Instructors
                    </InputLabel>
                    <div>
                        <Select autoWidth={true} className={classes.selectFieldInst}
                                name={"instructor"}
                                labelId="instructor-select-label"
                                id="instructor-select"
                                labelWidth={instructorLabelWidth}
                                value={videoInstructor}
                                onChange={onChangeHandler}
                        >
                            <MenuItem value={''}>
                                <em>Select An Instructor</em>
                            </MenuItem>
                            {fetchInstructors && fetchInstructors.map((iN, index) => {
                                return <MenuItem key={index} value={iN.name}>{iN.name}</MenuItem>
                            })}
                        </Select>
                        <IconButton>
                            <AddIcon fontSize="large" onClick={addInstructors}/>
                        </IconButton>
                    </div>
                </FormControl>
            </div>

            <div style={{textAlign: 'left', marginTop: '1%'}}>
                {videoInstructors.map((instructor, index) => {
                    return <Chip
                        key={index}
                        label={instructor.name}
                        style={{marginRight: '1%'}}
                        onDelete={handleDelete}
                        color="primary"
                    />
                })}
            </div>

            <div>
                <Typography variant="h4" className={classes.tag}> Tags </Typography>
                <FormControl fullWidth style={{textAlign: 'left', marginTop: "1%"}}>
                    <div>
                        <TextField
                            name={"tag"}
                            id="standard-basic"
                            label="Tag"
                            variant="outlined"
                            value={videoTag}
                            onChange={onChangeHandler}
                        />
                        <IconButton>
                            <AddIcon fontSize="large" onClick={addTags}/>
                        </IconButton>
                    </div>
                </FormControl>
            </div>

            <div style={{textAlign: 'left', marginTop: '1%'}}>
                {videoTags.map((tag, index) => {
                    return <Chip
                        key={index}
                        label={tag.name}
                        style={{marginRight: '1%'}}
                        onDelete={handleDelete}
                        color="primary"
                    />
                })}
            </div>

            <Button
                className={classes.button}
                variant="contained"
                size="large"
                color="secondary"
                fullWidth
                disableElevation
                onClick={onSubmit}
            >
                Add Video
            </Button>

            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={open}
                onClose={handleClose}
            >
                <div style={modalStyle} className={classes.modalPaper}>
                    <h2 id="simple-modal-title">Instructor Added Successfully</h2>

                    <Button onClick={handleClose}>Add Another</Button>
                    <Button onClick={() => {
                        history.push("/")
                    }}>OK</Button>
                </div>
            </Modal>
        </Container>
    )
}

export default withRoot(VimeoUpload);