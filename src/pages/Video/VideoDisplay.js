import React from 'react';
import withRoot from "../../withRoot";
import {makeStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import {Paper} from "@material-ui/core";
import Link from "@material-ui/core/Link";
import VideoWidget from "../../components/VideoWidget/VideoWidget";
import Container from "@material-ui/core/Container";

import HeroBg from '../../static/img/hero-bg.png';

const useStyles = makeStyles(theme => ({
    pageContainer: {
        backgroundImage: `url(${HeroBg})`
    },
    videoContainer: {
        paddingTop: theme.spacing(4),
        paddingLeft: theme.spacing(10),
        paddingRight: theme.spacing(10),

        // marginRight: theme.spacing(10)
        // marginLeft: theme.spacing(12),
        // marginRight: theme.spacing(12),
        // width: "fit-content"
    },
    videoFrame: {
        paddingLeft: theme.spacing(1),
        display: "flex",
        flexDirection: "column",
        minHeight: "50vh",
    },
    iFrame: {
        flexGrow: 1,
    },
    belowVideo: {
        paddingLeft: theme.spacing(1),
        // color: "white"
    },
    notesContainer: {
        textAlign: "left",

    },
    videoNotes: {
        borderLeft: "6px solid gray",
        minHeight: "50vh",
        // minWidth: "25vw",
        // maxWidth: 640,

    },
    videoMetadata: {
        marginTop: theme.spacing(1),
        borderLeft: "6px solid gray",
        minHeight: "25vh",
        paddingLeft: theme.spacing(1),
        textAlign: "left"
    },
    relatedVideos: {
        paddingLeft: theme.spacing(10),
        paddingRight: theme.spacing(10),
        textAlign: "left",
        marginTop: theme.spacing(8)
    }
}));

function VideoDisplay(props) {
    const classes = useStyles();
    let video = {
        length: "0:15",
        title: "Salsa",
        dancer: "Kate Vinshki"
    };

    return (
        <div className={classes.pageContainer}>
            <Grid container spacing={2} className={classes.videoContainer}>
                <Grid item xs={12}>
                    <Grid container justify="space-around" spacing={2}>
                        <Grid item xs={6}>
                            <div className={classes.videoFrame}>
                                <iframe
                                    className={classes.iFrame}
                                    src="https://player.vimeo.com/video/406119830"
                                    frameBorder="0"
                                    allow="autoplay; fullscreen" allowFullScreen>
                                </iframe>
                            </div>

                            <div className={classes.belowVideo}>
                                <Typography style={{textAlign: "left"}} variant="h6" color={"secondary"}>Benny and
                                    Ashley Salsa on 2 Turn pattern</Typography>

                                <Paper className={classes.videoMetadata}>
                                    <Typography variant="h6"> Instructors</Typography>
                                    <List dense={true}>
                                        <ListItem>
                                            <ListItemText
                                                primary="Single-line item"
                                                // secondary={true ? 'Secondary text' : null}
                                            />
                                        </ListItem>
                                        <ListItem>
                                            <ListItemText
                                                primary="Single-line item"
                                                // secondary={true ? 'Secondary text' : null}
                                            />
                                        </ListItem>
                                    </List>
                                    <Typography variant="h6"> Music</Typography>
                                </Paper>
                            </div>
                        </Grid>

                        <Grid item xs={4} className={classes.notesContainer}>
                            <Typography variant="h6" color={"secondary"}>Things to think abouts</Typography>
                            <Paper className={classes.videoNotes}>
                                <List dense={false}>
                                    <ListItem>
                                        <ListItemText
                                            primary="Single-line item"
                                            secondary={true ? 'Secondary text' : null}
                                        />
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="Single-line item"
                                            secondary={true ? 'Secondary text' : null}
                                        />
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="Single-line item"
                                            secondary={true ? 'Secondary text' : null}
                                        />
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="Single-line item"
                                            secondary={true ? 'Secondary text' : null}
                                        />
                                    </ListItem>
                                    <ListItem>
                                        <ListItemText
                                            primary="Single-line item"
                                            secondary={true ? 'Secondary text' : null}
                                        />
                                    </ListItem>
                                </List>
                            </Paper>
                        </Grid>
                    </Grid>
                </Grid>

                <Grid item xs={12} className={classes.relatedVideos}>
                    <div>
                        <Typography variant="h5" color="textPrimary">
                            <Link href="/" color="inherit" underline={"none"}> Related Videos </Link>
                        </Typography>
                        <hr/>
                    </div>

                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={6} md={3}>
                            <VideoWidget video={video}/>
                        </Grid>
                        <Grid item xs={12} sm={6} md={3}>
                            <VideoWidget video={video}/>
                        </Grid>
                        <Grid item xs={12} sm={6} md={3}>
                            <VideoWidget video={video}/>
                        </Grid>
                        <Grid item xs={12} sm={6} md={3}>
                            <VideoWidget video={video}/>
                        </Grid>
                    </Grid>

                </Grid>
            </Grid>
        </div>
    )
}

export default withRoot(VideoDisplay);