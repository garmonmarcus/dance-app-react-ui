import withRoot from "../../withRoot";
import React from 'react';
import Grid from "@material-ui/core/Grid";
import VideoWidget from "../../components/VideoWidget/VideoWidget";
import {Button} from "@material-ui/core";
import Link from "@material-ui/core/Link";
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import {addVideo, useFetchVideos} from "../../utils/api/apiFetchHooks";

import "./styles.css";


function HomeView() {

    let fetchVideos = useFetchVideos();

    console.log("Fetch Videos: ", fetchVideos);

    let video = {
        length: "0:15",
        title: "Salsa",
        dancer: "Kate Vinshki"
    };

    return (
        <Container >
            <div className="category-text" >
                <Typography variant="h5">
                    <Link href="/" underline={"none"} color="textPrimary"> Recently Added </Link>
                </Typography>
                <hr/>
            </div>

            <Grid container spacing={2}>
                <Grid item xs={12} sm={6} md={3}>
                    <VideoWidget video={video}/>
                </Grid>
            </Grid>

            <div className="category-text">
                <Typography variant="h5" color="textPrimary">
                    <Link href="/" underline={"none"} color="textPrimary"> Recommended For You </Link>
                </Typography>
                <hr/>
            </div>

            <Grid container spacing={3}>
                <Grid item xs={12} sm={6} md={3}>
                    <VideoWidget video={video}/>
                </Grid>
            </Grid>
        </Container>
    )
}

export default withRoot(HomeView);