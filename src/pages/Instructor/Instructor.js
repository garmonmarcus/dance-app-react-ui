import withRoot from "../../withRoot";
import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import HeroBg from "../../static/img/hero-bg.png";

const useStyles = makeStyles(theme => ({
    pageContainer: {
        backgroundImage: `url(${HeroBg})`,
        minHeight: "100vh"
    },

}));


function InstructorPage(){
    const classes = useStyles();

    return (
        <div className={classes.pageContainer}>

        </div>
    )
}

export default withRoot(InstructorPage);
