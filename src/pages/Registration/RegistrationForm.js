import withRoot from "../../withRoot";
import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';
import {Field, Form, FormSpy} from 'react-final-form';
import AppForm from "../../components/AppForm/AppForm";
import FormFeedback from "../../components/AppForm/FormFeedback";
import RFTextField from "../../components/AppForm/RFTextField";
import FormButton from "../../components/AppForm/FormButton";

import {fetchRegistration} from '../../utils/api/apiFetchHooks';


const useStyles = makeStyles(theme => ({
    form: {
        marginTop: theme.spacing(1),
    },
    signUpForm: {
        marginTop: theme.spacing(12),
    },
    button: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(2),
    },
    feedback: {
        marginTop: theme.spacing(2),
    },
}));

function RegistrationForm() {
    const classes = useStyles();

    const [sent, setSent] = React.useState(false);

    const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

    const onSubmit = async values => {
        setSent(true);

        await sleep(300);

        console.log("Printing values", values);
        // window.alert(JSON.stringify(values, 0, 2))

        let registrationMessage = fetchRegistration(values);
        console.log(registrationMessage);

        if (registrationMessage.message === "success") {
            return (
                <Link href="/" />
            )
        }
    };

    const required = value => (value ? undefined : 'Required')

    return (
        <div className={classes.signUpForm}>
            <Typography variant="h4" gutterBottom marked="center" align="center">
                Create your account
            </Typography>
            <AppForm>
                <Form onSubmit={onSubmit}
                      render={props => (
                          <form onSubmit={props.handleSubmit} className={classes.form} noValidate>
                              <Grid container spacing={2}>
                                  <Grid item xs={12} sm={6}>
                                      <Field
                                          component={RFTextField}
                                          // validate={required}
                                          label="First name"
                                          name="firstName"
                                          variant="outlined"
                                          fullWidth
                                          autoFocus
                                          required
                                      />
                                  </Grid>
                                  <Grid item xs={12} sm={6}>
                                      <Field
                                          component={RFTextField}
                                          // validate={required}
                                          fullWidth
                                          label="Last name"
                                          name="lastName"
                                          variant="outlined"
                                          required
                                      />
                                  </Grid>
                              </Grid>
                              <Field
                                  component={RFTextField}
                                  disabled={props.submitting || sent}
                                  fullWidth
                                  label="Email"
                                  margin="normal"
                                  name="email"
                                  variant="outlined"
                                  required
                              />
                              <Field
                                  fullWidth
                                  component={RFTextField}
                                  disabled={props.submitting || sent}
                                  name="password"
                                  label="Password"
                                  type="password"
                                  variant="outlined"
                                  margin="normal"
                                  required
                              />
                              {/*<FormSpy subscription={{submitError: true}}>*/}
                              {/*    {({submitError}) =>*/}
                              {/*        submitError ? (*/}
                              {/*            <FormFeedback className={classes.feedback} error>*/}
                              {/*                {submitError}*/}
                              {/*            </FormFeedback>*/}
                              {/*        ) : null*/}
                              {/*    }*/}
                              {/*</FormSpy>*/}

                              <FormButton
                                  className={classes.button}
                                  disabled={props.submitting || sent}
                                  size="large"
                                  color="secondary"
                                  fullWidth
                              >
                                  {props.submitting || sent ? 'In progress…' : 'Sign Up'}
                              </FormButton>
                          </form>
                      )}
                />
            </AppForm>
            <Typography variant="body2" align="center">
                <Link href="/login" underline="always">
                    Already have an account?
                </Link>
            </Typography>
            {/*<AppFooter />*/}
        </div>
    );
}

export default withRoot(RegistrationForm);