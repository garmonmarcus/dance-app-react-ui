import withRoot from "../../withRoot";
import React from 'react';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import {Form, FormSpy, Field} from 'react-final-form';
import AppForm from "../../components/AppForm/AppForm";
import FormButton from "../../components/AppForm/FormButton";
import RFTextField from "../../components/AppForm/RFTextField";
import Link from '@material-ui/core/Link';

import {fetchLogin} from '../../utils/api/apiFetchHooks';

const useStyles = makeStyles(theme => ({
    form: {
        background: theme.spacing(1),
    },
    signInForm: {
        marginTop: theme.spacing(12),
    },
    button: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(2),
    },
    feedback: {
        marginTop: theme.spacing(2),
    },
}));


function LoginForm() {
    console.log("Login Form");
    const classes = useStyles();
    const [sent, setSent] = React.useState(false);

    // const validate = values => {
    //     const errors = required(['email', 'password'], values);
    //
    //     if (!errors.email) {
    //         const emailError = email(values.email, values);
    //         if (emailError) {
    //             errors.email = email(values.email, values);
    //         }
    //     }
    //
    //     return errors;
    // };

    const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

    const onSubmit = async values => {
        setSent(true);

        await sleep(300);

        let loginSuccess = fetchLogin(values);
        console.log("Login Success Response :: ", loginSuccess);

        if (loginSuccess.message === "success") {
            return (
                <Link href="/"/>
            )
        }
    };

    return (
        <div className={classes.signInForm}>

            <Typography variant="h4" gutterBottom marked="center" align="center">
                Sign in to Salsa Academy
            </Typography>
            <AppForm>
                <Form onSubmit={onSubmit}
                      render={props => (
                          <form onSubmit={props.handleSubmit} className={classes.form} noValidate>
                              <Field
                                  autoComplete="email"
                                  autoFocus
                                  component={RFTextField}
                                  // disabled={submitting || sent}
                                  fullWidth
                                  label="Email"
                                  margin="normal"
                                  name="email"
                                  variant="outlined"
                                  required
                                  size="large"
                              />
                              <Field
                                  fullWidth
                                  size="large"
                                  required
                                  name="password"
                                  component={RFTextField}
                                  variant="outlined"
                                  label="Password"
                                  type="password"
                                  margin="normal"
                              />
                              {/*// <FormSpy subscription={{submitError: true}}>*/}
                              {/*// {({submitError}) =>*/}
                              {/*//     submitError ? (*/}
                              {/*//         <div></div>*/}
                              {/*//         // <FormFeedback className={classes.feedback} error>*/}
                              {/*//         //     {submitError}*/}
                              {/*//         // </FormFeedback>*/}
                              {/*//     ) : null*/}
                              {/*// }*/}
                              {/*// </FormSpy>*/}
                              <FormButton
                                  className={classes.button}
                                  variant="contained"
                                  disabled={props.submitting || sent}
                                  size="large"
                                  color="secondary"
                                  fullWidth
                                  disableElevation
                              >
                                  {props.submitting || sent ? 'In progress…' : 'Sign In'}
                              </FormButton>
                          </form>
                      )}
                />
            </AppForm>
            <Typography align="center">
                <Link underline="always" href="/registration">
                    Forgot password?
                </Link>
            </Typography>

        </div>
    )
}

export default withRoot(LoginForm);