export default {
    HOST: "http://localhost:8080"
    , REGISTRATION: "/v1/user/registration"
    , LOGIN_ENDPOINT: "/login"
    , INSTRUCTOR_ENDPOINT: "/v1/add/instructor"
    , FETCH_ALL_INSTRUCTORS: "/v1/instructors"
    , UPLOAD_VIDEO: "/v1/vimeo/upload"
    , FETCH_ALL_VIDEOS: "/v1/videos"

}