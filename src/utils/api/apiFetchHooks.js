import {useEffect, useState} from 'react';

import API_CONSTANTS from "./apiConstants"

export async function fetchRegistration(registrationForm) {
    const response = await fetch(API_CONSTANTS.HOST + API_CONSTANTS.REGISTRATION, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(registrationForm)
    });

    return await response.json();
}

export async function fetchLogin(loginForm){

    const response = await fetch(API_CONSTANTS.HOST + API_CONSTANTS.LOGIN_ENDPOINT, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(loginForm)
    });

    return await response.json();
}

export async function addInstructor(instructorForm){

    let header = {};

    console.log(JSON.stringify(instructorForm));

    const response = await fetch(API_CONSTANTS.HOST + API_CONSTANTS.INSTRUCTOR_ENDPOINT, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(instructorForm)
    });

    return await response.json();
}

export async function addVideo(formData){

    let header = {};

    // console.log(JSON.stringify(uploadedVideo));
    // console.log(uploadedVideo);

    const response = await fetch(API_CONSTANTS.HOST + API_CONSTANTS.UPLOAD_VIDEO, {
        method: 'POST',
        body: formData,
    });

    return await response.json();
}

export const useFetchInstructors = () =>{

    const [instructors, setInstructors] = useState();
    let header = {};


    useEffect(() => {
        fetch(API_CONSTANTS.HOST + API_CONSTANTS.FETCH_ALL_INSTRUCTORS, {
            method: 'GET',
        })
            .then(response => response.json())
            .then(data => {
                console.log("Instructors", data);
                setInstructors(data)
            })
    }, [setInstructors]);

    return instructors;
};

export const useFetchVideos = () =>{

    const [videos, setVideos] = useState();
    let header = {};


    useEffect(() => {
        fetch(API_CONSTANTS.HOST + API_CONSTANTS.FETCH_ALL_VIDEOS, {
            method: 'GET',
        })
            .then(response => response.json())
            .then(data => {
                // console.log("Video Data", data);
                setVideos(data[0].data)
            })
    }, [setVideos]);

    return videos;
};