import React from 'react';
import {useHistory, Redirect} from "react-router-dom";
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import CardActionArea from '@material-ui/core/CardActionArea';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import {red} from '@material-ui/core/colors';
import FavoriteIcon from '@material-ui/icons/Favorite';
import ShareIcon from '@material-ui/icons/Share';

import LizardImg from '../../static/img/contemplative-reptile.jpg';

const useStyles = makeStyles(theme => ({
    card: {
        // maxWidth: 345,
    },
    cardDisplay: {
        padding: theme.spacing(0),
        paddingBottom: theme.spacing(1),
        paddingLeft: theme.spacing(1),
        textAlign: "left",
        backgroundColor: "black"
    },
    cardData: {
      color: "white"
    },
    iconSmall: {
        padding: theme.spacing(1 / 2)
    },
    actionIcons: {
        textAlign: "right"
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
    },
    playIcon: {
        height: 38,
        width: 38,
        paddingBottom: "-50px",
        zIndex: 20
    },
}));

export default function VideoWidget(props) {
    const classes = useStyles();
    let history = useHistory();
    //
    const openVideo = () => {
        // return (<Redirect to={"/watch/video/10"} push />)
        history.push("/watch/video");
    };

    return (
        <React.Fragment>
            <Card className={classes.card} onClick={openVideo}>
                <CardActionArea >
                    <CardMedia
                        className={classes.media}
                        image={LizardImg}
                        title="Contemplative Reptile"
                        onClick={openVideo}
                    />
                </CardActionArea>
                <CardContent classes={{root: classes.cardDisplay}} gutterBottom>
                    <Typography variant="body2" className={classes.cardData} component="p">
                        {props.video.title} | {props.video.dancer}
                    </Typography>
                    <Typography variant="body2" className={classes.cardData} component="p">
                        {props.video.length}
                    </Typography>
                </CardContent>

                <div></div>
            </Card>
            <div className={classes.actionIcons}>
                <IconButton aria-label="add to favorites" classes={{root: classes.iconSmall}}>
                    <FavoriteIcon fontSize={"small"}/>
                </IconButton>
                <IconButton aria-label="share" classes={{root: classes.iconSmall}}>
                    <ShareIcon fontSize={"small"}/>
                </IconButton>
            </div>
        </React.Fragment>
    );
}