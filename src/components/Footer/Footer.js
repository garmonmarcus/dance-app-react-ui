import {makeStyles} from "@material-ui/core/styles";
import React from "react";


const useStyles = makeStyles(theme => ({
    border: {
        marginTop: theme.spacing(6),
        paddingTop: theme.spacing(6),
        // marginBottom: theme.spacing(1),
        borderTop: "1px solid #ededed"
    }
}));


function Footer(){

    const classes = useStyles();



    return(
        <div className={classes.border}>
            This is a footer!
        </div>
    )
}

export default Footer;