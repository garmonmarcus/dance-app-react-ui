import React from 'react';
import PropTypes from 'prop-types';
import TextField from "@material-ui/core/TextField";
import withRoot from "../../withRoot";

function RFTextField(props) {
    const {
        autoComplete,
        input,
        InputProps,
        meta: { touched, error, submitError },
        ...other
    } = props;

    return (
        <TextField
            color="secondary"
            error={Boolean(touched && (error || submitError))}
            {...input}
            {...other}
            InputProps={{
                inputProps: {
                    autoComplete,
                },
                ...InputProps,
            }}
            helperText={touched ? error || submitError : ''}
        />
    );
}

RFTextField.propTypes = {
    autoComplete: PropTypes.string,
    input: PropTypes.object.isRequired,
    InputProps: PropTypes.object,
    meta: PropTypes.shape({
        error: PropTypes.string,
        touched: PropTypes.bool.isRequired,
    }).isRequired,
};

export default withRoot(RFTextField);