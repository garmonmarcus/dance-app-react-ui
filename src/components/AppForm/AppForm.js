import React from 'react';
import PropTypes from 'prop-types';
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import { withStyles } from '@material-ui/core/styles';
import Paper from '../../components/Paper/Paper';

const styles = theme => ({
    root: {
        display: 'flex'
    },
    paper: {
        padding: theme.spacing(2, 1),
        [theme.breakpoints.up('md')]: {
            padding: theme.spacing(3, 3),
        },
    },
});

function AppForm(props) {
    const { children, classes } = props;

    return (
        <div className={classes.root}>
            <Container maxWidth="sm">
                <Box mt={2} mb={1}>
                    <Paper className={classes.paper}>{children}</Paper>
                </Box>
            </Container>
        </div>
    );
}

AppForm.propTypes = {
    children: PropTypes.node.isRequired,
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AppForm);