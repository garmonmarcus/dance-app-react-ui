import React, {useMemo} from 'react';
import {DropzoneArea} from 'material-ui-dropzone'

function VimeoDropzone(props) {

    return (
        <DropzoneArea
            onChange={props.onChange}
            onDrop={props.onChange}
            acceptedFiles={['video/*']}
            maxFileSize={2000000000}
            filesLimit={1}
            dropzoneText={"Drag and drop a video file here or click"}
            showFileNames={true}
            useChipsForPreview={false}
            // showPreviewsInDropzone={false}
        />
    )
}

export default VimeoDropzone;